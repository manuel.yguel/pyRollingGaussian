from .rollingGaussian import *

try:
    from .nDrollingGaussian import *
except ImportError:
    # numpy is probably not installed.
    try:
        import sys
        print("Error when importing nDrollingGaussian. nDrollingGaussian has a dependency on numpy. Is numpy installed on your machine ?", file=sys.stderr)
    except:
        # sys may be not available.
        pass