"""
Copyright (c) <2016> <Manuel Yguel, Strataggem R&D lab>

MIT Licence

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""
import numpy as np
from pyRingBuffer import RingBuffer

""" Compute mean and covariance of a multivariate Gaussian over
    a rolling window.
    tags: running mean, running average, rolling normal, rolling mean, rolling Gaussian
"""
class RollingGaussianNd:
    def __init__(self,N,dim=1):
        self.win = RingBuffer(N)
        self.dim=dim
        self.reset(N)
        self.volume_cst = np.sqrt((2*np.pi)**dim)

    def reset(self,M=None):
        """Reset the ring buffer used to compute the model.
        Reset the model parameters.
        """
        if( M is not None ):
            self.N = M
            self.norm = 1./M
        self.win.reset(M)
        self.sum = np.zeros(self.dim)
        self.sum_x2 = np.zeros((self.dim,self.dim))
        self.mean=np.zeros(self.dim)
        self.cov=np.zeros((self.dim,self.dim))

    def add(self,x):
        """Add a point to the rolling window.
        """
        prec = self.win.push(x)
        if( prec is not None ):
            self.sum += x-prec
            self.sum_x2 += np.dot(x.T,x)-np.dot(prec.T,prec)
        else:
            self.sum += x
            self.sum_x2 += np.dot(x.T,x)

    def updateModel(self):
        """Update the model parameters with the points in the rolling window.
        Returns the mean and the covariance of the points in the rolling window.
        """
        self.mean = self.norm * self.sum
        self.cov = self.norm*self.sum_x2 - np.dot(self.mean.T,self.mean)
        return (self.mean,self.cov)

    def update(self,x):
        """Add a point to the rolling window and update the model parameters
        accordingly. Returns the mean and the covariance of the points in the rolling window.
        """
        self.add(x)
        return self.updateModel()

    def stats(self):
        """Returns the eigenvalues, eigenvectors, the Nd standard deviation volume
        """
        (ei,ev)=np.linalg.eig(self.cov)
        det = np.linalg.det(self.cov)
        return (ei,ev,self.volume_cst*np.sqrt(det))


""" Compute mean and covariance of a multivariate Gaussian over
    a rolling window that size is growing until it reaches the target size.
    tags: running mean, running average, rolling normal, rolling mean, rolling Gaussian
"""
class RollingGaussianNdGrowingWindow:
    def __init__(self,N,dim=1):
        self.win = RingBuffer(N)
        self.dim=dim
        self.reset(N)
        self.volume_cst = np.sqrt((2*np.pi)**dim)
        self.current_size = 0

    def reset(self,M=None):
        """Reset the ring buffer used to compute the model.
        Reset the model parameters.
        """
        if( M is not None ):
            self.N = M
            self.norm = 1./M
        self.win.reset(M)
        self.sum = np.zeros(self.dim)
        self.sum_x2 = np.zeros((self.dim,self.dim))
        self.mean=np.zeros(self.dim)
        self.cov=np.zeros((self.dim,self.dim))
        self.current_size = 0

    def add(self,x):
        """Add a point to the rolling window.
        Compute the number of new points seen so far up to N
        """
        prec = self.win.push(x)
        if( prec is not None ):
            self.sum += x-prec
            self.sum_x2 += np.dot(x.T,x)-np.dot(prec.T,prec)
        else:
            self.sum += x
            self.sum_x2 += np.dot(x.T,x)
        if( self.current_size < self.N ):
            self.current_size += 1

    def updateModel(self):
        """Update the model parameters with the points in the rolling window.
        Returns the mean and the covariance of the points in the rolling window.
        """
        cnorm = None
        if( self.current_size < self.N ):
            cnorm = 1./self.current_size
        else:
            cnorm = self.norm
        self.mean = cnorm * self.sum
        self.cov = cnorm*self.sum_x2 - np.dot(self.mean.T,self.mean)
        return (self.mean,self.cov)

    def update(self,x):
        """Add a point to the rolling window and update the model parameters
        accordingly. Returns the mean and the covariance of the points in the rolling window.
        """
        self.add(x)
        return self.updateModel()

    def stats(self):
        """Returns the eigenvalues, eigenvectors, the Nd standard deviation volume
        """
        (ei,ev)=np.linalg.eig(self.cov)
        det = np.linalg.det(self.cov)
        return (ei,ev,self.volume_cst*np.sqrt(det))